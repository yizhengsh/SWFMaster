#include "../SWFTag.h"

TagFileAttributes::TagFileAttributes()
	: UseDirectBlit(false)
	, UseGPU(false)
	, HasMetadata(false)
	, ActionScript3(false)
	, UseNetwork(false)
{

}

void TagFileAttributes::setData(char* data, size_t len)
{
	SWFTag::setData(data, len);

}