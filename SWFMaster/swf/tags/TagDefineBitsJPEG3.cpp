#include "swf/SWFTag.h"
#include "swf/SWFInputStream.h"
#include "swf/SWFOutputStream.h"
#include <QtZlib/zlib.h>
#include "swf/utils/ImageHelper.h"

void TagDefineBitsJPEG3::setData( char* data, size_t len )
{
	SWFTag::setData(data, len);
}