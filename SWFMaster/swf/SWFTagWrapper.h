#ifndef SWFTagWrapper_h__
#define SWFTagWrapper_h__
#include <vector>
#include <string>
#include "SWFTypes.h"

class SWF;
class SWFFrame;
class SWFTag;

class SWFTagWrapper
{
private:
	SWFTag* _tag;
public:
	std::vector<SWFTagWrapper*> dependents;
public:
	static SWFTagWrapper* create(SWFTag* tag);

	SWFTagWrapper(SWFTag* tag);
	SWFTagWrapper();
	~SWFTagWrapper();

	void replace(SWFTag* tag);
	void onRefReplaced(SWFTag* oldTag, SWFTag* newTag);
	SWFTag* getTag() const;
	int getTagCode() const;

	std::string getTagName();
};

class SWFFrameWrapper
{
public:
	SWFTagWrapper* frameLabel;
	SWFTagWrapper* symbolClass;
	std::vector<SWFTagWrapper*> exportDefs;
	std::vector<SWFTagWrapper*> doABCs;
	std::vector<SWFTagWrapper*> imports;
	std::vector<SWFTagWrapper*> controlTags;

	std::vector<SWFTagWrapper*> buttons;
	std::vector<SWFTagWrapper*> binaryDatas;
	std::vector<SWFTagWrapper*> bitmaps;
	std::vector<SWFTagWrapper*> fonts;
	std::vector<SWFTagWrapper*> morphShapes;
	std::vector<SWFTagWrapper*> shapes;
	std::vector<SWFTagWrapper*> sounds;
	std::vector<SWFTagWrapper*> sprites;
	std::vector<SWFTagWrapper*> texts;
public:
	SWFFrameWrapper(SWFFrame* frame);
	~SWFFrameWrapper();
	SWFFrame* convertToFrame();
	void analyzeTagDependencies();
public:
	void getImageTags( std::vector<SWFTagWrapper*>& output) const;
	void getShapeTags( std::vector<SWFTagWrapper*>& output) const;
	void getSpriteTags( std::vector<SWFTagWrapper*>& output) const;
	void getFontTags( std::vector<SWFTagWrapper*>& output) const;
	void getTextTags( std::vector<SWFTagWrapper*>& output) const;
	void getButtonTags( std::vector<SWFTagWrapper*>& output) const;
	void getOtherTags( std::vector<SWFTagWrapper*>& output) const;
};

class SWFWrapper
{
public:
	UI16 frameRate;
	UI16 width;
	UI16 height;
public:
	SWFTagWrapper* fileAttributes;
	SWFTagWrapper* enableDebugger;
	SWFTagWrapper* enableDebugger2;
	SWFTagWrapper* metadata;
	SWFTagWrapper* scriptLimits;
	SWFTagWrapper* setBackgroundColor;
	SWFTagWrapper* defineSceneAndFrameLabelData;
	SWFTagWrapper* productInfo;
	SWFTagWrapper* protect;

	std::vector<SWFFrameWrapper*> frames;
public:
	SWFWrapper(SWF* swf);
	SWF* convertToSWF();
public:
	void getImageTags( std::vector<SWFTagWrapper*>& output) const;
	void getShapeTags( std::vector<SWFTagWrapper*>& output) const;
	void getSpriteTags( std::vector<SWFTagWrapper*>& output) const;
	void getFontTags( std::vector<SWFTagWrapper*>& output) const;
	void getTextTags( std::vector<SWFTagWrapper*>& output) const;
	void getButtonTags( std::vector<SWFTagWrapper*>& output) const;
	void getOtherTags( std::vector<SWFTagWrapper*>& output) const;
};
#endif // SWFTagWrapper_h__
