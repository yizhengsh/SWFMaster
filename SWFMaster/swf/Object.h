#ifndef Object_h__
#define Object_h__

#include <vector>

#define CREATE_FUNC(clazz) public: static clazz* create() { clazz* inst = new clazz(); inst->autorelease(); return inst; }
#define SAFE_RELEASE(obj) if (obj != nullptr) obj->release()
#define SAFE_RETAIN(obj) if (obj != nullptr) obj->retain()
#define SAFE_DELETE(obj) if (obj != nullptr) { delete obj; obj = nullptr }

class AutoreleasePool;

class Object
{
	friend class AutoreleasePool;
private:
	int _references;
	bool _autorelease;
public:
	Object();
	virtual ~Object();
public:
	void autorelease();
	void release();
	void retain();
};

class AutoreleasePool
{
	friend class Object;
private:
	static AutoreleasePool* _instance;
public:
	static AutoreleasePool* getInstance();
	AutoreleasePool()
		: _isPause(false)
	{

	}
	void check();
	void pause(bool isPause)
	{
		_isPause = isPause;
	}
private:
	void addObject(Object* o);
	void removeObject(Object* o);
private:
	std::vector<Object*> _pool;
private:
	bool  _isPause;
};
#endif // Object_h__
