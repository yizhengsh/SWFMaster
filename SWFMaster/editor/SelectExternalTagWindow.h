#ifndef SELECTEXTERNALTAGWINDOW_H
#define SELECTEXTERNALTAGWINDOW_H

#include <QDialog>

class SWFTreeModel;

namespace Ui {
class SelectExternalTagWindow;
}

class SelectExternalTagWindow : public QDialog
{
    Q_OBJECT

public:
    explicit SelectExternalTagWindow(QWidget *parent = 0);
    ~SelectExternalTagWindow();
public:
	void accept() override;
private:
    Ui::SelectExternalTagWindow *ui;
	SWFTreeModel* _swfObjectModel;
};

#endif // SELECTEXTERNALTAGWINDOW_H
