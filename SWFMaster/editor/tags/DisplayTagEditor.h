#ifndef _DisplayTagEditor_H
#define _DisplayTagEditor_H

#include <QWidget>
#include "swf/SWFTag.h"
#include "../Editor.h"

namespace Ui {
class DisplayTagEditor;
}

class SWF;
class SWFTagWrapper;

class DisplayTagEditor
	: public QWidget
	, public Editor
{
    Q_OBJECT

public:
    explicit DisplayTagEditor(SWFTagWrapper* wrapper, QWidget *parent = 0);
	~DisplayTagEditor();
	virtual const QString getEditorName() const override;
	virtual QWidget* asWidget() const override;
private slots:
	void onReplace();
	void onExport();
private:
	SWF* createSWF();
private:
    Ui::DisplayTagEditor *ui;
	SWFTagWrapper* _wrapper;
	DefineTag* _bits;
};

#endif // DEFINEBITSJPEG3EDITOR_H
