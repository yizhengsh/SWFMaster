#ifndef Icon_h__
#define Icon_h__
#include <QIcon>
#include <QPainter>
#include <map>
#include <string>
#include <assert.h>

static std::map<QString, QIcon*> _cachedIconMap;

#define MAKE_ICON(mName, mFile) public: static const QIcon& get##mName()\
	{\
		auto itr = _cachedIconMap.find(#mName);\
		if (itr == _cachedIconMap.end())\
		{\
			QIcon* icon = new QIcon(mFile);\
			_cachedIconMap[#mName] = icon;\
			return *icon;\
		}\
		return *(itr->second);\
	}

class Icon
{
	MAKE_ICON(Class, ":/resources/icon/as3/class_obj.png");
	MAKE_ICON(Interface, ":/resources/icon/as3/int_obj.png");
	MAKE_ICON(MethodPublic, ":/resources/icon/as3/methpub_obj.png");
	MAKE_ICON(MethodPrivate, ":/resources/icon/as3/methpri_obj.png");
	MAKE_ICON(MethodProtected, ":/resources/icon/as3/methpro_obj.png");
	MAKE_ICON(FieldPublic, ":/resources/icon/as3/field_public_obj.png");
	MAKE_ICON(Getter, ":/resources/icon/as3/methgettersetter_public_obj.png");
	MAKE_ICON(Setter, ":/resources/icon/as3/methgettersetter_public_obj.png");
	MAKE_ICON(DoABC, ":/resources/icon/as3/impc_obj.png");
	MAKE_ICON(Cpool, ":/resources/icon/package.png");
public:
	enum Kind
	{
		kClass = 1,
		kInterface,
		kMethod,
		kField,
		kGetter,
		kSetter
	};
	enum IconTrait
	{
		kPublic = 1,
		kPrivate = 1 << 1,
		kProtected = 1 << 2,
		kStatic = 1 << 3,
		kFinal = 1 << 4,
		kConstructor = 1 << 5,
		kOverride = 1 << 6,
		kConst = 1 << 7
	};
	static QIcon* getIcon(Kind kind, int ns = IconTrait::kPublic);
private:
	static bool isTrait(int n, IconTrait trait);
};

#endif // Icon_h__
